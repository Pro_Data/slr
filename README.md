# README #

This Repository includes a list of review papers studying the operating theater planning and scheduling problems. These review papers were published between 1976 and 2019.

### List of review papers ###

https://bitbucket.org/Pro_Data/slr/downloads/Review_Papers_-_Appendix.docx


### Review protocol ###

https://bitbucket.org/Pro_Data/slr/downloads/Protocol.docx